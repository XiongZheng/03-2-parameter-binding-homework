# 作业要求

## Definition of Done

* 必须非常严格的按照题目中的定义进行实现。
* 程序必须能够在真实 HTTP Client 下工作。
* 必须有单元或者集成测试。每一个测试要代表一个 Task，每一个测试通过之后需要进行一次 commit（小步提交）。
* 测试至少要覆盖业务要求提到的问题。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。

## 业务要求

现在中国的教育市场非常巨大，儿童的教（碎）育（币）投（能）资（力）得到了充分的重视。因此创业公司 “**愛颩尅谂**” 公司也立志推出面向超级小学生的教育产品。

当然做软件是非常昂贵的，因此产品也要以迭代的方式进行发布，快速试探市场的动态决定下一步的方向。目前第一期的目标如下。

作为一个小学生，我肯定非常希望快速得到一张可以打印的加法表和乘法表。这样我就可以打印下来作为参考了。我们的服务以 **API** 的形式提供。（重要，谁要是用 Page Template 的方式提供了我就 Fire 掉 TA，“愛颩尅谂” 公司的 CTO 这样说）

## Story 1 生成加法表

首先我们应当满足加法表的生成业务。目前的业务要求如下

**AC1：加法表的 API 必须以如下的形式提供**

该 API 的 Endpoint 定义如下：

|项目|说明|
|---|---|
|API Endpoint|*/api/tables/plus*|
|HTTP Method|GET|

该 API 的返回结果定义如下：

|项目|说明|
|---|---|
|Status Code|200|
|Content-Type|*text/plain*|

其 Body 包含纯文本表示的加法表的全部内容。

**AC2：加法表的每一列必须都向左对齐，并且每一列和下一列的最小间隔必须为一个空格**

例如这样：

```
4+4=8
5+4=9  5+5=10
6+4=10 6+5=11 6+6=12
```

**AC3 必须打印从 1 到 9 的加法表**

## Story 2 生成乘法表

目前的业务要求如下

**AC1：乘法表的 API 必须以如下的形式提供**

该 API 的 Endpoint 定义如下：

|项目|说明|
|---|---|
|API Endpoint|*/api/tables/multiply*|
|HTTP Method|GET|

该 API 的返回结果定义如下：

|项目|说明|
|---|---|
|Status Code|200|
|Content-Type|*text/plain*|

其 Body 包含纯文本表示的乘法表的全部内容。

**AC2：乘法表的每一列必须都向左对齐，并且每一列和下一列的最小间隔必须为一个空格**

例如这样：

```
2*2=4
3*2=6  3*3=9
4*2=8  4*3=12 4*4=16
5*2=10 5*3=15 5*4=20 5*5=25
```

**AC3：必须打印从 1 到 9 的乘法表**

## Story 3 生成指定范围内的加法表和乘法表

**AC1：指定范围内的加法和乘法表**

现在我们已经不能满足于每一次的时候生成的加法表和乘法表都是固定范围的了。因此我们期望指定加法表和乘法表的起始和结束数字。因此加法表和乘法表的 API 相应的更改为如下的形式：

|项目|说明|
|---|---|
|API Endpoint|*/api/tables/plus?start={start}&end={end}*|
|HTTP Method|GET|

以及

|项目|说明|
|---|---|
|API Endpoint|*/api/tables/multiply?start={start}&end={end}*|
|HTTP Method|GET|

其中 

* Story 1 和 Story 2 的 AC 仍然需要满足。
* `start`，`end` 必须为正整数。并且 `start` 小于或者等于 `end`。
* 必须输出从 `start` 到 `end` 的加法表和乘法表。

> 以加法表为例，如果 `start` 为 3，`end` 为 5 则输出为：

```
3+3=6
4+3=7 4+4=8
5+3=8 5+4=9 5+5=10
```

**AC2：非法参数的处理**

如果参数不符合题目的要求，例如为不为正整数，或大小关系不符合要求。或其他你可以考虑到的无法生成加法或者乘法表的状况。则统一返回如下的 Response

|项目|说明|
|---|---|
|Statu Code|400|

## Story 4 互动解答

打印加法乘法表还只是开始。我们要加入一些互动功能。我们要加入题目验证的功能。

**AC1：题目的验证**

|项目|说明|
|---|---|
|API Endpoint|*/api/check*|
|HTTP Method|POST|
|Content-Type|application/json|

其请求的 body 定义如下：

```json
{
  "operandLeft": number,
  "operandRight": number,
  "operation": "+" or "*",
  "expectedResult": number
}
```

其响应的格式如下：

|项目|说明|
|---|---|
|Status Code|200|
|Content-Type|application/json|

```json
{
  "correct": true or false
}
```

**AC2：参数的验证**

请求中的 JSON 中的所有 field 都需要提供，如果有一个没有提供则为无效的参数。同时，如果类型不正确也为无效参数。当参数无效的时候，该 API 的响应格式为：

|项目|说明|
|---|---|
|Status Code|400|

## Story 5 不等式互动解答

我们知道，在教育培训领域教学过程中往往只教学单个的知识点，但是在练习的时候往往要几个知识点联系运用。因此我们也在 Story 4 的基础上加入了不等式的验证。

**AC1：题目的验证**

|项目|说明|
|---|---|
|API Endpoint|*/api/check*|
|HTTP Method|POST|
|Content-Type|application/json|

其请求的 body 定义如下：

```json
{
  "operandLeft": number,
  "operandRight": number,
  "operation": "+" or "*",
  "expectedResult": number,
  "checkType": "=" or "<" or ">"
}
```

其中如果 `"checkType"` 没有提供，则默认为 "=" 以便保证 API 的兼容性。

其响应的格式仍然如下：

|项目|说明|
|---|---|
|Status Code|200|
|Content-Type|application/json|

```json
{
  "correct": true or false
}
```

**AC2：参数的验证**

请求中的 JSON 中的所有 field 都需要提供，如果有一个没有提供则为无效的参数。同时，如果类型不正确也为无效参数。当参数无效的时候，该 API 的响应格式为：

|项目|说明|
|---|---|
|Status Code|400|